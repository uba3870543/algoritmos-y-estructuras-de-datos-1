def promedio(lu: str) -> float:
    archivo = open('./diccionarios/alumnos.csv', 'r', encoding="utf8")
    lineas = archivo.readlines()
    lista_final=[]
    cantidad_de_materias: int=0
    suma_de_notas: float=0
    #deja cada linea del csv en una lista, y cada elemento es cada parte atrás de la coma
    for linea in lineas:
        dato_de_alumno=linea.split(",")
        #elimina el \n de cada lista
        dato_de_alumno[-1] = dato_de_alumno[-1].rstrip('\n')
        #transforma el dato de la nota en float
        dato_de_alumno[-1] = float(dato_de_alumno[-1])

        lista_final.append(dato_de_alumno)

    for dato_actual in lista_final:
        if lu == dato_actual[0]:
            suma_de_notas+=dato_actual[-1]
            cantidad_de_materias+=1
    return suma_de_notas / cantidad_de_materias

