from queue import LifoQueue as Pila
import random

#8
def generar_nros_al_azar(n: int, desde: int, hasta: int) -> Pila:
    lista_de_enteros=[]
    p: Pila = Pila()
    for _ in range(0,n):
        p.put(random.randint(desde,hasta))
    return p

#9
def cantidad_de_elementos(p: Pila) -> int:
    
    contenido=[]
    contador: int=0
    while not(p.empty()):
        contenido.append(p.get())
        contador+=1
    for elemento in contenido[::-1]:
        p.put(elemento)
    return contador

#10
def buscar_el_maximo(p: Pila) -> int:
    maximo: int=0
    contenido=[]
    while not p.empty():
        contenido.append(p.get())
        if contenido[-1] > maximo:
            maximo = contenido[-1]
    for elemento in contenido[::-1]:
        p.put(elemento)
    return maximo

#11
def esta_bien_balanceada(s: str) -> bool:
    p = Pila()
    parentesis_cerrar: int=0
    parentesis_abrir: int=0
    for caracter in s[::-1]:
        p.put(caracter)

    while not p.empty():
        p_actual = p.get()
        if p_actual == "(":
            parentesis_abrir+= 1
        elif p_actual == ")":
            parentesis_cerrar+= 1
        #chequea que ")" esté cerrando una operación:
        if p_actual == ")" and parentesis_abrir < parentesis_cerrar:
            return False
        
    if (parentesis_abrir - parentesis_cerrar) == 0:
        return True
    else:
        return False

#12
def postfix(s: str) -> int:
    p = Pila()
    operando_1: int=0
    operando_2: int=0
    tokens = s.split(" ")

    for token in tokens:
        if not (token in "+-*/"):
            p.put(int(token))
        else:
            operando_2 = p.get()
            operando_1 = p.get()
            if token == "+":
                p.put(operando_1 + operando_2)
            elif token == "-":
                p.put(operando_1 - operando_2)
            elif token == "*":
                p.put(operando_1 * operando_2)
            elif token == "/":
                p.put(operando_1 / operando_2)
    return p.get()