import random
from queue import LifoQueue as Pila
from promedio_alumnos import promedio

#19
def agrupar_por_longitud(nombre_archivo: str) -> dict:
    archivo = open(nombre_archivo,'r')
    resultado: dict[int]={}

    for linea in archivo:
        palabras = linea.split()
        #Deja solo letras, luego crea o agrega a la lista su longitud
        for palabra in palabras:
            palabra = palabra.strip(".,;:")
            longitud = len(palabra)

            if longitud in resultado:
                resultado[longitud]+=1
            else:
                resultado[longitud]=1
    return resultado

#20
def diccionario_de_promedios(nombre_archivo: str) -> dict:
    archivo = open(nombre_archivo,'r', encoding="utf8")
    resultado: dict={}
    lineas = archivo.readlines()

    for alumno in lineas:
        alumno = alumno.split(",")
        alumno = alumno[0]

        if not alumno in resultado:
            resultado[alumno] = promedio(alumno)

    return resultado

#21
def palabra_mas_frecuente(nombre_archivo: str) -> str:
    archivo = open(nombre_archivo,'r')
    lista_de_palabras = []
    diccionario_de_palabras: dict={}

    #Creo una lista con las palabras
    for linea in archivo:
        palabras = linea.split()

        #Dejo solo caracteres en minuscula
        for palabra in palabras:
            palabra = palabra.strip(',.¿?¡!:;')
            palabra = palabra.lower()

            #si existe suma 1, sino, crea 1
            if palabra in diccionario_de_palabras:
                diccionario_de_palabras[palabra]+=1
            else:
                diccionario_de_palabras[palabra]=1
        
        #busco el maximo:
        maximo_numero: int=0
        maximo_palabra= None
        for clave, valor in diccionario_de_palabras.items():
            if valor > maximo_numero:
                maximo_numero = valor
                maximo_palabra = clave

        return maximo_palabra


historial_de_navegacion = [
                            ('usuario1', 'https://www.ejemplo1.com'),
                            ('usuario1', 'https://www.ejemplo2.com'),
                            ('usuario2', 'https://www.ejemplo3.com'),
                            ('usuario2', 'https://www.ejemplo4.com'),
                            ('usuario3', 'https://www.ejemplo5.com'),
                            ('usuario3', 'https://www.ejemplo6.com'),
                            ('usuario1', 'https://www.ejemplo3.com'),
                            ('usuario1', 'https://www.ejemplo4.com'),
                            ('usuario2', 'https://www.ejemplo5.com'),
                            ('usuario2', 'https://www.ejemplo6.com'),
                            ('usuario3', 'https://www.ejemplo7.com'),
                            ('usuario3', 'https://www.ejemplo8.com')
                            ]
#22.1 (definición pedía solo diccionario, esta función implementa lista de tuplas->diccionario)
def historiales(lista_de_paginas:[str,str]) -> dict:
    historial: dict[str,Pila()]={}

    #0 es el user, 1 es el sitio
    for usuario in lista_de_paginas:
            if not usuario[0] in historial.keys():
                historial[usuario[0]] = Pila()
                historial[usuario[0]].put(usuario[1])
            else:
                historial[usuario[0]].put(usuario[1])

    return historial

#22.2
def visitar_sitio(historiales: dict, usuario: str, sitio:str):
    historiales[usuario].put(sitio)

    return historiales

#variable atras/adelante
sitio_a_mover = Pila()
#22.3
def navegar_atras(historiales: dict,usuario: str):
    global sitio_a_mover
    sitio_a_mover.put(historiales[usuario].get())
    return historiales

#22.4
def navegar_adelante(historiales:dict,usuario:str):
    global sitio_a_mover
    historiales[usuario].put(sitio_a_mover.get())
    return historiales

#Para probarlo agregar y sacar atras/adealante (no hay test)
#salida2 = navegar_atras(historiales(historial_de_navegacion),'usuario2')
#salid1 = navegar_atras(salida2,'usuario2')
#salida = navegar_adelante(salida1,'usuario2')
#lista_salida: [str]=[]
#while not salida['usuario2'].empty():
#    lista_salida.append(salida['usuario2'].get())
#print(lista_salida)

#23
inventario = {
    'Producto1': {'nombre': 'Producto1', 'precio': 10.99, 'cantidad': 100},
    'Producto2': {'nombre': 'Producto2', 'precio': 15.50, 'cantidad': 50},
    'Producto3': {'nombre': 'Producto3', 'precio': 5.99, 'cantidad': 200},
    'Producto4': {'nombre': 'Producto4', 'precio': 25.00, 'cantidad': 75},
    'Producto5': {'nombre': 'Producto5', 'precio': 8.99, 'cantidad': 150},
    'Producto6': {'nombre': 'Producto6', 'precio': 12.75, 'cantidad': 120},
    'Producto7': {'nombre': 'Producto7', 'precio': 19.99, 'cantidad': 60},
    'Producto8': {'nombre': 'Producto8', 'precio': 6.49, 'cantidad': 180},
    'Producto9': {'nombre': 'Producto9', 'precio': 22.99, 'cantidad': 90},
    'Producto10': {'nombre': 'Producto10', 'precio': 14.25, 'cantidad': 110}
}
#23.1
def agregar_producto(inventario: dict, nombre: str, precio: float, cantidad: int):
    if not nombre in inventario.keys():
        inventario[nombre] = {'nombre' : nombre, 'precio' : precio, 'cantidad' : cantidad }
    return inventario

#23.2
def actualizar_stock(inventario: dict, nombre: str, cantidad: int):
    inventario[nombre]['cantidad'] = cantidad
    return inventario

#23.3
def actualizar_precios(inventario: dict, nombre: str, precio: int):
    inventario[nombre]['precio'] = precio
    return inventario

#23.4
def calcular_valor_inventario(inventario: dict) -> float:
    resultado: int=0
    for clave in inventario.keys():
        resultado+= inventario[clave]['cantidad'] * inventario[clave]['precio']
    return resultado

print(calcular_valor_inventario(inventario))
