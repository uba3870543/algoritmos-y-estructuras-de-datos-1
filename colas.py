from queue import Queue as Cola
import random
from pilas import generar_nros_al_azar
#13
def generar_nros_al_azar_cola(n: int, desde: int, hasta: int) -> Cola:
    c = Cola()
    p = generar_nros_al_azar(n,desde,hasta)
    for i in range(0,n):
        c.put(p.get())
    return c

#14
def cantidad_de_elementos_cola(c: Cola) -> int:
    contenido=[]
    contador: int=0
    while not c.empty():
        contenido.append(c.get())
        contador+=1
    for i in contenido:
        c.put(i)
    return contador

#15
def buscar_el_maximo(c: Cola) -> int:
    contenido=[]
    maximo_actual: int = 0

    while not c.empty():
        numero_actual= c.get()
        contenido.append(numero_actual)
        if numero_actual > maximo_actual:
            maximo_actual = numero_actual
    
    for i in contenido:
        c.put(i)

    return maximo_actual

#16.1
def armar_secuencia_bingo() -> Cola:
    c = Cola()
    lista: list[int]=[]
    for i in range(0,100):
        lista.append(i)
    random.shuffle(lista)
    for i in lista:
        c.put(i)
    return c
#16.2
def jugar_carton_de_bingo(carton: list[int], bolillero:Cola[int]) -> int:
    carton_lleno: int=0
    jugadas_totales: int=0
    bolillero_aux: list[int]=[]
    #suma hasta cartón lleno
    while carton_lleno < 12:
        bolilla = bolillero.get()
        if bolilla in carton:
            carton_lleno+=1
        jugadas_totales+=1
        #guarda la bolilla en otro bolillero
        bolillero_aux.append(bolilla)

    #termina de vaciar el bolillero
    while not bolillero.empty():
        bolilla = bolillero.get()
        bolillero_aux.append(bolilla)

    #pasa del bolillero auxiliar al original
    for i in bolillero_aux:
        bolillero.put(i)
    return jugadas_totales
#no hice unit test
#print(jugar_carton_de_bingo([1, 6, 69, 67, 56, 58, 48, 95, 80, 63, 18, 98], armar_secuencia_bingo()))

#para armar los cartones
def carton_de_bingo() -> list[int]:
    carton = list=[]
    for i in range(12):
        carton.append(random.randint(0,99))
    print(carton)

#17
def n_pacientes_urgentes(c: Cola[int,str,str]) -> int:
    pacientes_urgentes: int=0
    cola_aux: Cola[int,str,str]=Cola()

    while not c.empty():
        paciente = c.get()
        cola_aux.put(paciente)
        if paciente[0] <= 3:
            pacientes_urgentes+=1
    
    #rearmo la cola:
    while not cola_aux.empty():
        paciente_aux = cola_aux.get()
        c.put(paciente_aux)
    
    return pacientes_urgentes

#18
#inputs: ("Nombre y apellido", DNI, ¿Preferencial?, ¿Discapacitado?)
#Prioridad: Discapacidad=True > Preferencial=True > Discapacidad=Preferencial=False
def a_clientes(c:Cola[str,int,bool,bool]) -> Cola[str,int,bool,bool]:
    contenido: list=[]
    cola_discapacitados: Cola[str,int,bool,bool]=Cola()
    cola_preferencial: Cola[str,int,bool,bool]=Cola()
    cola_corriente: Cola[str,int,bool,bool]=Cola()
    cola_final: Cola[str,int,bool,bool]=Cola()
    #Define tipo de paciente en 3 colas
    while not c.empty():
        paciente = c.get()
        contenido.append(paciente)
        if paciente[3] == True:
            cola_discapacitados.put(paciente)
        elif paciente[2] == True:
            cola_preferencial.put(paciente)
        else:
            cola_corriente.put(paciente)
    #Ingresa discapacitados
    while not cola_discapacitados.empty():
        paciente = cola_discapacitados.get()
        cola_final.put(paciente)
    #Ingresa preferenciales
    while not cola_preferencial.empty():
        paciente = cola_preferencial.get()
        cola_final.put(paciente)
    #Ingresa resto
    while not cola_corriente.empty():
        paciente = cola_corriente.get()
        cola_final.put(paciente)
    #Rearma la cola
    for paciente in contenido:
        c.put(paciente)
        
    return cola_final
