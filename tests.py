import unittest
from queue import LifoQueue as Pila
from queue import Queue as Cola
from pilas import cantidad_de_elementos, buscar_el_maximo, esta_bien_balanceada, postfix
from colas import cantidad_de_elementos_cola, buscar_el_maximo, n_pacientes_urgentes, a_clientes
from diccionarios import agrupar_por_longitud, diccionario_de_promedios, palabra_mas_frecuente, historiales, visitar_sitio

#tests pilas
@unittest.skip
class TestCantidadDeElementos(unittest.TestCase):
    def test_cantidad_de_elementos_con_pila_vacia(self):
        pila = Pila()
        resultado = cantidad_de_elementos(pila)
        self.assertEqual(resultado, 0)

    def test_cantidad_de_elementos_con_pila_novacia(self):
        pila = Pila()
        pila.put(1)
        pila.put(2)
        pila.put(3)
        resultado = cantidad_de_elementos(pila)
        self.assertEqual(resultado, 3)
@unittest.skip
class TestBuscarElMaximo(unittest.TestCase):
    def test_buscar_el_maximo_inicio(self):
        p = Pila()
        p.put(20)
        p.put(15)
        p.put(18)
        p.put(6)
        resultado = buscar_el_maximo(p)
        self.assertAlmostEqual(resultado, 20)

    def test_buscar_el_maximo_medio(self):
        p = Pila()
        p.put(31)
        p.put(15)
        p.put(37)
        p.put(23)
        resultado = buscar_el_maximo(p)
        self.assertAlmostEqual(resultado, 37)

    def test_buscar_el_maximo_final(self):
        p = Pila()
        p.put(31)
        p.put(15)
        p.put(37)
        p.put(70)
        resultado = buscar_el_maximo(p)
        self.assertAlmostEqual(resultado, 70)
@unittest.skip
class TestEstaBienBalanceado(unittest.TestCase):
    def test_esta_bien_balanceada_true_a(self):
        s = "1 + ( 2 x 3 = ( 2 0 / 5 ) )"
        resultado = (esta_bien_balanceada(s))
        self.assertEqual(resultado,True)

    def test_esta_bien_balanceada_true_b(self):
        s = "0 * ( 1 + ( 2 * ( -1)))"
        resultado = (esta_bien_balanceada(s))
        self.assertEqual(resultado,True)

    def test_esta_bien_balanceada_false(self):
        s = "1 + ) 2 x 3 ( ( )"
        resultado = (esta_bien_balanceada(s))
        self.assertEqual(resultado,False)
@unittest.skip
class TestPostfix(unittest.TestCase):
    def test_postfix_1(self):
        s = "3 4 + 5 * 2 -"
        resultado = postfix(s)
        self.assertEqual(resultado,33)

    def test_postfix_2(self):
        s = "5 2 * 8 4 / +"
        resultado = postfix(s)
        self.assertEqual(resultado,12)

    def test_postfix_3(self):
        s = "4 2 / 5 5 * +"
        resultado = postfix(s)
        self.assertEqual(resultado,27)

#tests colas
@unittest.skip
class TestCantidadDeElementosCola(unittest.TestCase):
    def test_cantidad_de_elementos_cola_vacia(self):
        c = Cola()
        resultado = cantidad_de_elementos_cola(c)
        self.assertEqual(resultado,0)

    def test_cantidad_de_elementos_cola_noVacia(self):
        c = Cola()
        c.put(100)
        c.put(199)
        c.put(400)
        resultado = cantidad_de_elementos_cola(c)
        self.assertEqual(resultado,3)
@unittest.skip
class TestBuscarElMaximo(unittest.TestCase):
    def test_buscar_el_maximo_inicio(self):
        c= Cola()
        c.put(23)
        c.put(7)
        c.put(20)
        resultado = buscar_el_maximo(c)
        self.assertEqual(resultado,23)

    def test_buscar_el_maximo_medio(self):
        c= Cola()
        c.put(23)
        c.put(70)
        c.put(20)
        resultado = buscar_el_maximo(c)
        self.assertEqual(resultado,70)

    def test_buscar_el_maximo_final(self):
        c= Cola()
        c.put(23)
        c.put(21)
        c.put(34)
        resultado = buscar_el_maximo(c)
        self.assertEqual(resultado,34)
@unittest.skip
class TestNPacientesUrgentes(unittest.TestCase):
    def test_n_pacientes_urgentes_1(self):
        c = Cola()
        datos_pacientes = [
            (1, "Juan Pérez", "Ninguna"),
            (2, "María García", "Hipertensión"),
            (3, "Carlos Rodríguez", "Diabetes"),
            (4, "Ana López", "Asma"),
            (5, "Pedro Sánchez", "Ninguna"),
            (6, "Laura Martínez", "Ninguna"),
            (7, "Miguel González", "Alergias"),
            (8, "Isabel Torres", "Ninguna"),
            (9, "Javier Fernández", "Ninguna"),
            (10, "Luisa Ramírez", "Ninguna"),
            (1, "Manuel Pérez", "Ninguna"),
            (2, "María González", "Ninguna"),
            (3, "Sofía López", "Ninguna"),
            (4, "Diego Sánchez", "Ninguna"),
            (5, "Elena Martínez", "Ninguna"),
            ]
        for i in datos_pacientes:
            c.put(i)
        resultado = (n_pacientes_urgentes(c))
        self.assertEqual(resultado, 6)
@unittest.skip
class TestAClientes(unittest.TestCase):
    def test_a_clientes(self):
        c = Cola()
        c_esperada = Cola()
        c.put(("Juan Pérez", 123456789, True, True))
        c.put(("Ana López", 987654321, True, False))
        c.put(("Carlos González", 456789123, False, False))
        c.put(("María Rodríguez", 789123456, False, True))
        c.put(("Luisa Martínez", 654321987, True, False))
        resultado = a_clientes(c)
        lista_esperada = [("Juan Pérez", 123456789, True, True),
                          ("María Rodríguez", 789123456, False, True),
                          ("Ana López", 987654321, True, False),
                          ("Luisa Martínez", 654321987, True, False),
                          ("Carlos González", 456789123, False, False)]
        for i in lista_esperada:
            c_esperada.put(i)
        self.assertEqual(resultado.queue,c_esperada.queue)

#tests diccionarios
@unittest.skip
class TestAgruparPorLongitud(unittest.TestCase):
    def test_agrupar_por_longitud(self):
        archivo = "./diccionarios/agrupar_por_longitud.txt"
        resultado = agrupar_por_longitud(archivo)
        self.assertDictEqual(resultado, {5: 3, 3: 1, 4: 2, 11: 1, 10: 1})
@unittest.skip
class TestDiccionarioDePromedios(unittest.TestCase):
    def test_diccionario_de_promedios(self):
        archivo = './diccionarios/alumnos.csv'
        resultado = diccionario_de_promedios(archivo)
        esperado = {'1/23': 5.0, '2/23': 3.5, '3/23': 9.5, '4/23': 5.0, '5/23': 4.0}
        self.assertDictEqual(resultado, esperado)
@unittest.skip
class TestPalabraMasFrecuente(unittest.TestCase):
    def test_palabra_mas_frecuente(self):
        archivo = './diccionarios/palabra_mas_frecuente.txt'
        resultado = palabra_mas_frecuente(archivo)
        esperado = 'buenas'
        self.assertEqual(resultado,esperado)

@unittest.skip
class TestRegistroWeb(unittest.TestCase):

    def test_historiales(self):
        historial_de_navegacion = [
                            ('usuario1', 'https://www.ejemplo1.com'),
                            ('usuario1', 'https://www.ejemplo2.com'),
                            ('usuario2', 'https://www.ejemplo3.com'),
                            ('usuario2', 'https://www.ejemplo4.com'),
                            ('usuario3', 'https://www.ejemplo5.com'),
                            ('usuario3', 'https://www.ejemplo6.com')]
        salida = historiales(historial_de_navegacion)
        lista_salida: [str]=[]
        for valor in salida.values():
            while not valor.empty():
                lista_salida.append(valor.get())

        resultado = lista_salida
        esperado = ['https://www.ejemplo1.com',
                    'https://www.ejemplo2.com',
                    'https://www.ejemplo3.com',
                    'https://www.ejemplo4.com',
                    'https://www.ejemplo5.com',
                    'https://www.ejemplo6.com']
        self.assertCountEqual(resultado,esperado)

    def test_visitar_sitio(self):
        historial_de_navegacion = [
                            ('usuario1', 'https://www.ejemplo1.com'),
                            ('usuario1', 'https://www.ejemplo2.com'),
                            ('usuario2', 'https://www.ejemplo3.com'),
                            ('usuario2', 'https://www.ejemplo4.com'),
                            ('usuario3', 'https://www.ejemplo5.com'),
                            ('usuario3', 'https://www.ejemplo6.com')]
        salida = visitar_sitio(historiales(historial_de_navegacion),'usuario1','www.hola.com')
        lista_salida: [str]=[]
        for valor in salida.values():
            while not valor.empty():
                lista_salida.append(valor.get())

        resultado = lista_salida
        esperado = ['https://www.ejemplo1.com',
                    'https://www.ejemplo2.com',
                    'https://www.ejemplo3.com',
                    'https://www.ejemplo4.com',
                    'https://www.ejemplo5.com',
                    'https://www.ejemplo6.com',
                    'www.hola.com']
        self.assertCountEqual(resultado,esperado)



if __name__ == '__main__':
    unittest.main()